# Maintainer: Anjandev Momi <anjan@momi.ca>
# Contributor: Maarten van Gompel <proycon@anaproy.nl>
pkgname=sxmo-dmenu
pkgver=5.0.12
pkgrel=0
pkgdesc="Dmenu fork for Sxmo UI; supports highlight, centering, volume-key navigation and more"
url="https://git.sr.ht/~mil/sxmo-dmenu"
arch="all"
license="MIT"
makedepends="libx11-dev libxinerama-dev libxft-dev"
options="!check" # has no tests
subpackages="$pkgname-doc"
provides="dmenu"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~mil/sxmo-dmenu/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -i -e '/CFLAGS/{s/-Os//;s/=/+=/}' \
		-e '/LDFLAGS/{s/=/+=/}' \
		"$builddir"/config.mk
}

build() {
	make X11INC=/usr/include/X11 \
		X11LIB=/usr/lib/X11 \
		FREETYPEINC=/usr/include/freetype2 \
		-C "$builddir"
}

package() {
	make DESTDIR=$pkgdir PREFIX=/usr \
		-C "$builddir" install
}

sha512sums="
972d1866d8bec20a6721426b4690d03596e8d00b22fdb2d92d9bf7220b694d0a170e351b4e39fe76cdbc002ee70dad7523d0e7e5fa1dc98d2f09111eb2d0541e  sxmo-dmenu-5.0.12.tar.gz
"
